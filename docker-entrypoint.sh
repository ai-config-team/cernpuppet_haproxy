#!/bin/sh
set -e

# Start rsyslogd
rm -f /var/run/rsyslogd.pid
rsyslogd

addgroup -g $(ls -g /var/run/docker.sock | awk '{print $3}') dockerh
usermod -a -G dockerh haproxy

# first arg is `-f` or `--some-option`
if [ "${1#-}" != "$1" ]; then
  set -- haproxy "$@"
fi

if [ "$1" = 'haproxy' ]; then
  shift # "haproxy"
  # if the user wants "haproxy", let's add a couple useful flags
  #   -W  -- "master-worker mode" (similar to the old "haproxy-systemd-wrapper"; allows for reload via "SIGUSR2")
  #   -db -- disables background mode
  set -- haproxy -W -db "$@"
fi

exec "$@"
