FROM haproxy:1.8.14-alpine

LABEL org.label-schema.maintainer="David Moreno García" \
      org.label-schema.name="HAProxy"

RUN set -x && \
    apk upgrade --update && \
    apk add ca-certificates docker rsyslog shadow socat --no-cache && \
    mkdir -p /etc/rsyslog.d && \
    mkdir -p /var/lib/rsyslog && \
    addgroup -S haproxy && \
    adduser -S -G haproxy -H -D haproxy && \
    touch /var/log/haproxy.log && \
    ln -sf /dev/stdout /var/log/haproxy.log

COPY docker-entrypoint.sh /

COPY rsyslog.conf /etc

ENTRYPOINT [ "/docker-entrypoint.sh" ]

CMD [ "haproxy", "-f", "/etc/haproxy/haproxy.cfg" ]
